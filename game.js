var express = require('express');
var player = require('./player');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname+ '/www'));

io.on('connection', function(socket){
	console.log('new player connected');
	socket.on('disconnect', function() {
		console.log('player disconnected');
	});
	socket.on('player start', function(msg, ships){
		var player = new Player(msg);
		console.log('new player: ' + player.name);
	});
});

http.listen(8080, function() {
	console.log('listening on *.8080');
})
//constructor

function Game(playerA, time, mode) {
	this.playerA = playerA;
	this.time = time;
	this.mode = mode;
}

Game.prototype = {
	start: function() {
	},
	update: function() {
	},
	end: function() {
	}
}

function Player(name, ships, email) {
	this.name = name;
	this.ships = ships;
	this.email = email;
};

Player.prototype = {
	wasHit: function(shot) {
		return this.ships.some(
			function(value) {
				if (value.washit(shot))
					return true;
				else
					return false;
			}, this);
	},
	start: function() {
		console.log("player starts game");
	}
}

function Ship(size, position) {
	this.size = size;
	this.position = position;
	this.hits = [];
	this.status = "2";
};

Ship.prototype = {
	wasHit: function(shot) {
		return
		this.position.some( function(value) {
			if (value.x == shot.x && value.y == shot.y) {
				this.hits.push(value.x, value.y);
				if (this.hits.length == this.position.length) {
					this.status = "0";
				} else {
					this.status = "1";
				}
				return true;
			} else {
				return false;
			}
		}, this);
	}
}
