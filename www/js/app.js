function generateGrid() {
	var carrier = new Ship(5);
	var battleship = new Ship(4);
	var cruiser = new Ship(3);
	var submarine = new Ship(3);
	var destroyer = new Ship(2);
	console.log(carrier.toString());
	console.log(battleship.toString());
	console.log(cruiser.toString());
	console.log(submarine.toString());
	console.log(destroyer.toString());
	var ships = [carrier, battleship, cruiser, submarine, destroyer];
	var grid = [
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0]
	]

	ships.forEach( function(item, index, array) {
		var placed = false;
		while (!placed) {
			var xPos = randomPosition();
			var yPos = randomPosition();
			if (grid[yPos+1][xPos+1] === 0) {
				var orientation = randomOrientation();

				placed = true;
			}
		}

		item.position.x = randomPosition();
		item.position.y = randomPosition();
		item.orientation = randomOrientation();
	});

};

function fitsOrientation(orientation, shipSize, position) {
	switch(orientation) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
	}
	return true;
};

function randomPosition() {
	return Math.floor(Math.random() * (10 - 1)) + 1;
};

function randomOrientation() {
	return Math.floor(Math.random() * (4 - 1)) + 1;
}

function Ship(size, position) {
	this.size = size;
	this.position = position;
	this.orientation = "";
	this.hits = [];
	this.status = "2";
};

Ship.prototype = {
	wasHit: function(shot) {
		return
		this.position.some( function(value) {
			if (value.x == shot.x && value.y == shot.y) {
				this.hits.push(value.x, value.y);
				if (this.hits.length == this.position.length) {
					this.status = "0";
				} else {
					this.status = "1";
				}
				return true;
			} else {
				return false;
			}
		}, this);
	},
	toString: function() {
		return JSON.stringify(this);
	}
}


$(function () {
	var socket = io();

	$('form').submit(function(){
		socket.emit('player start', $('#name').val());
		return false;
	});

});
